package com.twuc.webApp.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.twuc.webApp.Contract;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class ParameterControllerTest {

    private ObjectMapper objectMapper;

    @BeforeEach
    void setup(){
        objectMapper = new ObjectMapper();
    }

    @Autowired
    private MockMvc mockMvc;

    // test section 2.1
    @Test
    void should_verify_path_variable_type() throws Exception {
        mockMvc.perform(get("/api/users/3"))
                .andExpect(content().string("user 3"));
    }

    @Test
    void should_verify_path_variable_primitive_type() throws Exception {
        mockMvc.perform(get("/api/users1/3"))
                .andExpect(status().isOk())
                .andExpect(content().string("user 3"));
    }

    @Test
    void should_verify_two_path_variables_type() throws Exception {
        mockMvc.perform(get("/api/users/3/books/1"))
                .andExpect(status().isOk())
                .andExpect(content().string("book 1 of user 3"));
    }

    // test section 2.2
    @Test
    void should_verify_query_string_is_bind_on_param() throws Exception {
        mockMvc.perform(get("/api/users")
                .param("userId","3")
                .param("bookId","1")
                )
                .andExpect(status().isOk())
                .andExpect(content().string("book 1 of user 3"));
    }

    // test section 2.3
    @Test
    void should_serialize_contract() throws IOException {
        String contractStr = "{\"name\":\"Tony\",\"telPhone\":18012345678}";
        Contract contract = objectMapper.readValue(contractStr, Contract.class);
        assertEquals(contract.getName(), "Tony");
        assertEquals(contract.getTelPhone(), Long.valueOf(18012345678L));
    }

    @Test
    void should_deserialize_contract_json() throws JsonProcessingException {
        Contract contract = new Contract();
        contract.setName("Tony");
        contract.setTelPhone(18012345678L);
        contract.setYearOfBirth(1002);
        String contractStr = objectMapper.writeValueAsString(contract);
        assertEquals(contractStr, "{\"name\":\"Tony\",\"telPhone\":18012345678,\"yearOfBirth\":1002}");
    }

    @Test
    void should_return_date() throws Exception {
        mockMvc.perform(post("/api/datetimes")
        .content("{\"dateTime\":\"2019-10-01T10:00:00Z\"}")
        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk())
            .andExpect(jsonPath("$.dateTime").value("2019-10-01T10:00:00Z"));

    }

    // test section 2.4

    @Test
    void should_verify_not_null() throws Exception {
        mockMvc.perform(post("/api/contracts")
            .content("{\"name\":\"Tony\",\"telPhone\":18012345678,\"yearOfBirth\":1002}")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(content().string("Tony"));
    }

    @Test
    void should_verify_null() throws Exception {
        mockMvc.perform(post("/api/contracts")
                .content("{\"telPhone\":18012345678}")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void should_verify_size() throws Exception {
        mockMvc.perform(post("/api/contracts")
                .content("{\"name\":\"TonyThomas\",\"telPhone\":18012345678}")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void should_verify_max() throws Exception {
        mockMvc.perform(post("/api/contracts/yearOfBirth")
                .content("{\"name\":\"Tony\",\"telPhone\":18012345678,\"yearOfBirth\":99999}")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void should_verify_min() throws Exception {
        mockMvc.perform(post("/api/contracts/yearOfBirth")
                .content("{\"name\":\"Tony\",\"telPhone\":18012345678,\"yearOfBirth\":100}")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void should_verify_invalid_email() throws Exception {
        mockMvc.perform(post("/api/contracts/email")
                .content("{\"name\":\"Tony\",\"telPhone\":18012345678,\"yearOfBirth\":100,\"email\":\"zzzz\"}")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void should_verify_valid_email() throws Exception {
        mockMvc.perform(post("/api/contracts/email")
                .content("{\"name\":\"Tony\",\"telPhone\":18012345678,\"yearOfBirth\":1002,\"email\":\"389786573@qq.com\"}")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk());
    }
}
