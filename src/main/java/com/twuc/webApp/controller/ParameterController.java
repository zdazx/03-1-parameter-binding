package com.twuc.webApp.controller;

import com.twuc.webApp.Contract;
import com.twuc.webApp.DateTime;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import java.time.ZonedDateTime;

@RestController
public class ParameterController {

    // test section 2.1
    @GetMapping("/api/users/{userId}")
    public String getUserWithId(@PathVariable Integer userId){
        return "user " + userId;
    }

    @GetMapping("/api/users1/{userId}")
    public String getUserWithPrimitiveId(@PathVariable int userId){
        return "user " + userId;
    }

    @GetMapping("/api/users/{userId}/books/{bookId}")
    public String getUserAndBookWithId(@PathVariable int userId, @PathVariable int bookId){
        return "book " + bookId + " of user " + userId;
    }

    // test section 2.2
    @GetMapping("/api/users")
    public String getUserAndBookWithIdInRequestParam(@RequestParam int userId,
                                                     @RequestParam int bookId){
        return "book " + bookId + " of user " + userId;
    }

    // test section 2.3
    @PostMapping("/api/datetimes")
    public DateTime getDateTime(@RequestBody DateTime dateTime){
        return dateTime;
    }

    // test section 2.4
    @PostMapping("/api/contracts")
    public String getContracts(@RequestBody @Valid Contract contract){
        return contract.getName();
    }

    @PostMapping("/api/contracts/yearOfBirth")
    public int getYearOfBirth(@RequestBody @Valid Contract contract){
        return contract.getYearOfBirth();
    }

    @PostMapping("/api/contracts/email")
    public String getEmail(@RequestBody @Valid Contract contract){
        return contract.getEmail();
    }
}
