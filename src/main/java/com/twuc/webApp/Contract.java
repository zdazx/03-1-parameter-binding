package com.twuc.webApp;

import javax.validation.constraints.*;

public class Contract {
    @NotNull
    @Size(max = 6,min = 1)
    private String name;
    private Long telPhone;
    @Max(9999)
    @Min(1000)
    private int yearOfBirth;
    @Email
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTelPhone(Long telPhone) {
        this.telPhone = telPhone;
    }

    public Long getTelPhone() {
        return telPhone;
    }

}
